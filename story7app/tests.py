import time
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from . import views
from .models import *
from .views import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class UnitTest(TestCase):
	
	def test_home_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_home_using_right_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'home.html')

	def test_calling_right_views_function(self):
		found = resolve('/')
		self.assertEqual(found.func, views.home)

	def test_template_contain_Activity_Experience_Achievement(self):
		request = HttpRequest()
		response = views.home(request)
		self.assertContains(response, 'My Activities')
		self.assertContains(response, 'My Experiences')
		self.assertContains(response, 'My Achievements')
		self.assertContains(response, 'My Educations')

	def test_lab8_using_search_books_template(self):
	    response = Client().get('/search_books/')
	    self.assertTemplateUsed(response, 'search_books.html')