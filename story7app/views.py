import requests
import json
from django.shortcuts import render
from django.http import JsonResponse

def home(request):
    return render(request, 'home.html')

def search_books(request):
	return render(request,'search_books.html')	