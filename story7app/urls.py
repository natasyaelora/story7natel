from django.contrib import admin
from django.urls import path
from story7app import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    path('search_books/', views.search_books, name='search_books'),
]